#!/usr/bin/python2.7
# EASY-INSTALL-SCRIPT: 'BreakSeq2==-2.2-','run_breakseq2.py'
__requires__ = 'BreakSeq2==-2.2-'
import pkg_resources
pkg_resources.run_script('BreakSeq2==-2.2-', 'run_breakseq2.py')
