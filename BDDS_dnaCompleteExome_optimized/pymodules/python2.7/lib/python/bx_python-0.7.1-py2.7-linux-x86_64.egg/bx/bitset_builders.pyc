ó
9Tc           @   s¥   d  Z  d d l m Z d d l Td d l Z d d d d d d i  d	  Z d d d d d d i  d
  Z d d d d d d d  Z g  d  Z d d d d  Z	 d S(   sÔ   
Support for creating dictionaries of `Bitset`s / `BinnedBitset`s from text
files containg sets of "covered" intervals in sequences (e.g. `BED`_ files).

.. BED: http://genome.ucsc.edu/FAQ/FAQformat.html#format1
iÿÿÿÿ(   t   warn(   t   *Ni    i   i   i   c         C   sx  d } d }	 t   }
 x\|  D]T} | j d  s | j   rC q n  | j   } d } t |  | k r | | d k r d } q n  | | } | | k rê | |
 k r× | | k r¾ | | } n t } t |  |
 | <n  | } |
 | }	 n  t | |  t | |  } } | r't	 d | |  } n  | rCt
 | | |  } n  | | k r\t d  n  |	 j | | |  q W|
 S(   s¹  
    Read a file into a dictionary of bitsets. The defaults arguments 
    
    - 'f' should be a file like object (or any iterable containing strings)
    - 'chrom_col', 'start_col', and 'end_col' must exist in each line. 
    - 'strand_col' is optional, any line without it will be assumed to be '+'
    - if 'lens' is provided bitset sizes will be looked up from it, otherwise
      chromosomes will be assumed to be the maximum size
    t   #t   +t   -i    s   Interval start after end!N(   t   Nonet   dictt
   startswitht   isspacet   splitt   lent   MAXt   BinnedBitSett   intt   maxt   minR    t	   set_range(   t   ft	   chrom_colt	   start_colt   end_colt
   strand_colt   upstream_padt   downstream_padt   lenst
   last_chromt   last_bitsett   bitsetst   linet   fieldst   strandt   chromt   sizet   startt   end(    (    sr   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/bx_python-0.7.1-py2.7-linux-x86_64.egg/bx/bitset_builders.pyt   binned_bitsets_from_file   s:    
	 
!   c         C   sï  d
 } d
 }	 t   }
 d } xÍ|  D]Å} | j d  s" | j   rI q" n  | j d  r^ q" n  | j d  r² t j d |  } | r" | j d  r" t | j d   } q" q" n  | j   } d } t	 |  | k rò | | d k rò d } qò n  | | } | | k rY| |
 k rF| | k r-| | } n t
 } t |  |
 | <n  | } |
 | }	 n  t | |  | t | |  | } } | rt d | |  } n  | rºt | | |  } n  | | k rÓt d	  n  |	 j | | |  q" W|
 S(   s¹  
    Read a file into a dictionary of bitsets. The defaults arguments 
    
    - 'f' should be a file like object (or any iterable containing strings)
    - 'chrom_col', 'start_col', and 'end_col' must exist in each line. 
    - 'strand_col' is optional, any line without it will be assumed to be '+'
    - if 'lens' is provided bitset sizes will be looked up from it, otherwise
      chromosomes will be assumed to be the maximum size
    i    R   t   browsert   tracks   offset=(\d+)i   R   R   s   Interval start after end!N(   R   R   R   R   t   ret   searcht   groupR   R	   R
   R   R   R   R   R    R   (   R   R   R   R   R   R   R   R   R   R   R   t   offsetR   t   mR   R   R   R    R!   R"   (    (    sr   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/bx_python-0.7.1-py2.7-linux-x86_64.egg/bx/bitset_builders.pyt   binned_bitsets_from_bed_file1   sJ    
	 
)   c         C   s¡  d } d } t   }	 x|  D]}}
 |
 j d  r7 q n  |
 j   } d } t |  | d k r{ | | d k r{ d } q{ n  | | } | | k rÃ | |	 k r° t t  |	 | <n  | } |	 | } n  t | |  t | |  } } | d k r+| rt d | |  } n  | r+t	 t | |  } q+n  | d k rr| rSt	 t | |  } n  | rrt d | |  } qrn  | | d k r | j
 | | |  q q W|	 S(   s(   Read a file into a dictionary of bitsetsR   R   i   R   i    N(   R   R   R   R	   R
   R   R   R   R   R   R   (   R   R   R   R   R   t   upstreamt
   downstreamR   R   R   R   R   R   R   R!   R"   (    (    sr   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/bx_python-0.7.1-py2.7-linux-x86_64.egg/bx/bitset_builders.pyt   binned_bitsets_proximityd   s>    	  
!    c         C   s§   d } d } t   } x |  D] } | d } | | k rj | | k rW t t  | | <n  | } | | } n  t | d  t | d  } } | j | | |  q W| S(   s(   Read a list into a dictionary of bitsetsi    i   i   N(   R   R   R   R   R   R   (   t   listR   R   R   t   lR   R!   R"   (    (    sr   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/bx_python-0.7.1-py2.7-linux-x86_64.egg/bx/bitset_builders.pyt   binned_bitsets_from_list   s    	
!c   
      C   s   t  t  } xw |  D]o } | j d  r. q n  | j   } | | | k r t | |  t | |  } }	 | j | |	 |  q q W| S(   s'   Read a file by chrom name into a bitsetR   (   R   R   R   R	   R   R   (
   R   R   R   R   R   t   bitsetR   R   R!   R"   (    (    sr   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/bx_python-0.7.1-py2.7-linux-x86_64.egg/bx/bitset_builders.pyt   binned_bitsets_by_chrom   s     !(
   t   __doc__t   warningsR    t	   bx.bitsetR&   R#   R+   R.   R1   R3   (    (    (    sr   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/bx_python-0.7.1-py2.7-linux-x86_64.egg/bx/bitset_builders.pyt   <module>   s   
%3